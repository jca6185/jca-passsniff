# PassSniff V1.0

Script realizado en Python para capturar contraseñas de conexiones HTTP tras realizar un ataques de ARP Spoofing

![1](snifferpass.png)


## Installation

##### install python
Ubuntu/Debian:
```
# apt-get install python
```
##### install python Python package manager
Ubuntu/Debian:
```
# apt-get install python3-pip
```
##### Clone the repository
```
# git clone https://gitlab.com/jca6185/jca-passsniff.git
# cd jca-passsniff
# pip -r requeriments.txt
```
## How to use

Antes de usar el script y basado en el grafico antes mostrado, es conveniente que tenga claro cual es la IP de la Victima, Cual es la IP de la Puerta de Enlace y cual es la Interfaz de red desde donde esta haciendo el ataques

El comando general es:

#### python jca-passsniff.py -t IPVICTIMA -i INTERFAZ -g IPGATEWAY

Ejemplo de USO
```
# python jca-passsniff.py -t 192.168.1.50 -i eth0 -g 192.168.1.254
```
Puede interrumpir su ejecución presionado las teclas CRTL+C

Esto genera un archivo llamado pass.json que contiene las credenciales capturadas.

## Funcional Version
Este script es la version 1.0 (release date: 2022/28/06) y ha sido probada en la version de Kali Linux 2022.2

## Copyright
this repository forked from [jca6185/jca-passsniff](https://gitlab.com/jca6185/jca-passsniff).  

## Agradecimientos
Este trabajo fue realizado para la Especialización en Ciberseguridad de la Universidad Catolica de Manizales, un especial agradecimientos al profesor Cesar Muñoz @kascesar por su paciencia, dedicación y esfuerzo para con nosotros.  

# Only use Academic.



